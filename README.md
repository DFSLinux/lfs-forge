# LFS Forge
is a set of scripts designed to build a **L**inux **F**rom **S**cratch system, basicly _**forging**_ a Linux Operating System from the ground up.

This set of scripts is based on scripts written by [Marcel van den Boer, LFScript](https://www.lfscript.org). The original scripts have NOT been updated or upgraded since 2017. This has been an on going problem since version 4, he even disappeared from the project for such a long time, that many thought the project was abandoned. Noone was really able to do much. Even now with version 4 out for quit a few years, noone really knows how to use the *factory* scripts. There's basic documentation, but missing important stuff. Development - who knows where that takes place, very secret. Nothing really comes from the Launchpad questions that come up. 

So for **ALL** of the above mishappenings, I have decided to use the scripts as a base. I will remove anything that *Doesn't work for me* stuff.


## Features
* Builds simple package archives (XZ compressed TAR archives)
* Logs all compilation and installation messages to one file per package
* Includes a script to build a Live CD from your system. 
* 32 bit & 64 bit builds


## Planned Features
* Package Manager
* Live Installer
  * CLI
  * GUI
* Branding tools

## Documentation
Most of the documenting of this project will be available in the [WIKI](../../wikis/home).

## Bugs, Issues, Requests, etc.
Have a question, feedback, request? Found a bug? Have an issue with something?
You can use the [ISSUES AREA](../../issues) for any/all of these.

## License
MIT License. For details, please read the [LICENSE](LICENSE) file.

## Other
Host System:
* [SalixOS](https://www.salixos.org/) (Recommended)
* [Debian](https://www.debian.org/)   (Suggested - requires many pkgs)
* [Ubuntu](https://ubuntu.com/)       (Suggested - requires many pkgs)
